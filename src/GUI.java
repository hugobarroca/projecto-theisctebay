import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class GUI {

	private JFrame frame;
	private User client;
	private DefaultListModel<String> files = new DefaultListModel<String>();
	private JList<String> filesList = new JList<String>(files);

	public GUI(User client) {
		this.client = client;
		frame = new JFrame("TheIscteBay");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		addFrameContent();
		frame.pack();
		frame.setVisible(true);
	}

	private void addFrameContent() {
		frame.setLayout(new BorderLayout());

		JPanel northPanel = new JPanel();
		northPanel.setLayout(new BorderLayout());

		JLabel searchLabel = new JLabel("Texto a procurar:");

		JTextField message = new JTextField();
		message.setPreferredSize(new Dimension(150, 25));

		JButton searchButton = new JButton("Procurar");
		searchButton.setPreferredSize(new Dimension(120, 25));
		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearFilesList();
				client.searchFiles(message.getText());
			}
		});

		northPanel.add(searchLabel, BorderLayout.WEST);
		northPanel.add(message, BorderLayout.CENTER);
		northPanel.add(searchButton, BorderLayout.EAST);

		JPanel southPanel = new JPanel();
		southPanel.setLayout(new BorderLayout());

		JPanel downloadPanel = new JPanel(new GridLayout(2, 1));

		JButton downloadButton = new JButton("Descarregar");
		downloadButton.setPreferredSize(new Dimension(120, 50));
		downloadButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				client.consult();
			}
		});

		JProgressBar progressbar = new JProgressBar();
		progressbar.setPreferredSize(new Dimension(120, 50));

		downloadPanel.add(downloadButton);
		downloadPanel.add(progressbar);

		southPanel.add(filesList, BorderLayout.CENTER);
		southPanel.add(downloadPanel, BorderLayout.EAST);

		frame.add(northPanel, BorderLayout.NORTH);
		frame.add(southPanel, BorderLayout.CENTER);
	}

	private void clearFilesList() {
		files.clear();
	}
	
	public void addFiles(String file) {
		files.addElement(file);
	}

}
