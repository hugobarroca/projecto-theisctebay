import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

public class User {

	private String username;
	private InetAddress address;
	private int serverPort;
	private int userPort;
	private String filesPath;
	private ArrayList<String> peersConnected;
	private ArrayList<String> filesname;
	private File[] files;
	private Socket socket;
	private Socket peerSocket;
	private BufferedReader serverIn;
	private PrintWriter serverOut;
	private BufferedReader peerIn;
	private PrintWriter peerOut;
	private GUI gui;
	
	//Testando o Git (Again, yes).
	public User(String address, String serverPort, String userPort, String filesPath) {
		try {
			this.address = InetAddress.getByName(address);
			this.serverPort = Integer.parseInt(serverPort);
			this.userPort = Integer.parseInt(userPort);
			this.filesPath = filesPath;
			this.username = JOptionPane.showInputDialog("Qual o seu nome de usuário?");
			this.files = new File(filesPath).listFiles();
			filesname = new ArrayList<String>();
			peersConnected = new ArrayList<String>();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		try {
			connectToServer();
			subscribeDirectory();
			serverOut.println(username);
		} catch (IOException e) {
			e.printStackTrace();
		}
		gui = new GUI(this);
	}

	public void startServing() throws IOException {
		ServerSocket s = new ServerSocket(userPort);
		System.out.println("Lançou UserSocket: " + s);
		try {
			while (true) {
				Socket socket = s.accept();
				System.out.println("Conexão aceite: " + socket);
				new DealWithPeer(socket, this, files).start();
			}
		} finally {
			s.close();
		}
	}
	
	private void connectToServer() throws IOException {
		System.out.println("Endereço = " + address);
		socket = new Socket(address, serverPort);

		System.out.println("Socket = " + socket);
		serverIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		serverOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
	}
	
	private void connectToPeer(InetAddress address, int port) throws IOException {
		System.out.println("Endereço = " + address);
		peerSocket = new Socket(address, port);

		System.out.println("Socket = " + peerSocket);
		peerIn = new BufferedReader(new InputStreamReader(peerSocket.getInputStream()));
		peerOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(peerSocket.getOutputStream())), true);
	}
	
	public void subscribeDirectory() {
		serverOut.println("INSC");
		serverOut.println("INSC " + toString());
	}
	
	public void consult() {
		peersConnected.clear();
		serverOut.println("CLT");
		try {
			/*JFrame userList = new JFrame("Users Connected");
			userList.setLayout(new BorderLayout());
			JTextArea userTextArea = new JTextArea();
			userList.add(userTextArea, BorderLayout.CENTER);
			userTextArea.setPreferredSize(new Dimension(250, 100));
			userList.pack();*/
			String user = serverIn.readLine();
			while (!user.equals("END")) {
				peersConnected.add(user);
				user = serverIn.readLine();
				//userTextArea.append(user + "\n");
			}
			//userList.setVisible(true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void searchFiles(String search) {
		filesname.clear();
		consult();
		if(!peersConnected.isEmpty())
			for(String peer : peersConnected) {
				String peerSplit[] = peer.split(" ");
				String peerAddress = peerSplit[1];
				int peerPort = Integer.parseInt(peerSplit[2]);
				if(peerPort != userPort) {
					try {
						connectToPeer(InetAddress.getByName(peerAddress), peerPort);
						peerOut.println("SEARCH");
						peerOut.println(search);
						String file = peerIn.readLine();
						//Perceber porque o break não terminava o ciclo
						while (!file.equals("END")) {
							if(!filesname.contains(file))
								filesname.add(file);
							file = peerIn.readLine();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		for(String name : filesname)
			gui.addFiles(name);
	}
	
	public static void main(String[] args) {
		User client = new User(args[0], args[1], args[2], args[3]);
		try {
			client.startServing();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		return username + " " + socket.getLocalAddress().toString().substring(1) + " "
				+ userPort;
	}

}
