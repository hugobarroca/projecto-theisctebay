import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Directory {

	private int PORT;
	private ArrayList<String> connectedUsers;

	public Directory(String port) {
		PORT = Integer.parseInt(port);
		connectedUsers = new ArrayList<String>();
	}

	public ArrayList<String> getConnectedClients() {
		return connectedUsers;
	}

	public void addClient(String userToAdd) {
		connectedUsers.add(userToAdd);
	}

	public void removeClient(String userToRemove) {
		connectedUsers.remove(userToRemove);
	}

	public void startServing() throws IOException {
		ServerSocket s = new ServerSocket(PORT);
		System.out.println("Lançou ServerSocket: " + s);
		try {
			while (true) {
				Socket socket = s.accept();
				System.out.println("Conexão aceite: " + socket);
				new DealWithUser(socket, this).start();
			}
		} finally {
			s.close();
		}
	}

	public static void main(String[] args) {
		try {
			new Directory(args[0]).startServing();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
