import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class DealWithPeer extends Thread {

	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;
	private User peer;
	private File[] files;

	public DealWithPeer(Socket socket, User peer, File[] files) {
		this.socket = socket;
		this.peer = peer;
		this.files = files;
		try {
			doConnections();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void doConnections() throws IOException {
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
	}
	
	public void showFiles(String search) {
		for(File f : files) { 
			if(f.getName().contains(search))
				out.println(f.getName());
			//System.out.println(f.getName());
		}
		out.println("END");
	}
	
	
	@Override
	public void run() {
		String consultListener = null;
		while (!interrupted()) {
			try {
				consultListener = in.readLine();
				System.out.println(consultListener);
				if (consultListener != null) {
					switch (consultListener) {
					case "SEARCH":
						System.out.println("Entrou no case");
						showFiles(in.readLine());
						System.out.println("Mostrou os ficheiros");
						break;
					}
				}
			} catch (IOException e) {
			}
		}
	}
}
