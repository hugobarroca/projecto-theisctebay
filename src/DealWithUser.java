import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class DealWithUser extends Thread {

	private Socket socket;
	private Directory server;
	private String user;
	private BufferedReader in;
	private PrintWriter out;
	private String username;

	public DealWithUser(Socket socket, Directory server) {
		this.socket = socket;
		this.server = server;
		try {
			doConnections();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void clientsConsult() {
		System.out.println("O usuário " + username + " requisitou uma consulta dos outros usuários");
		for (String client : server.getConnectedClients()) {
			String[] cltClient = client.split(" ");
			String cltClientNew = "CLT " + cltClient[2] + " " + cltClient[3];
			out.println(cltClientNew);
		}
		out.println("END");
	}

	private void addClientToDirectory() {
		try {
			user = in.readLine();
			server.addClient(user);
			username = in.readLine();
			System.out.println("O usuário " + username + " está agora conectado");
		} catch (IOException e) {
		}
	}

	private void doConnections() throws IOException {
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
	}

	@Override
	public void run() {
		String consultListener = null;
		while (!interrupted()) {
			try {
				consultListener = in.readLine();
				if (consultListener != null) {
					switch (consultListener) {
					case "INSC": 
						addClientToDirectory();
						break;
					case "CLT":
						clientsConsult();
						break;
					case "SEARCH":
						requestFiles();
						break;
					}
				}
			} catch (IOException e) {
			}
		}
	}

	private void requestFiles() {
		
	}

}
